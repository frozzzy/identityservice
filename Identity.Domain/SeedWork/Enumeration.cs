﻿using System.Reflection;

namespace Identity.Domain.SeedWork;

public abstract class Enumeration : IComparable
{
    public string Name { get; private set; }

    public int Id { get; private set; }

    protected Enumeration(int id, string name) => (Id, Name) = (id, name);

    public override string ToString() => Name;

    public static IEnumerable<T> GetAll<T>() where T : Enumeration =>
        typeof(T).GetFields(BindingFlags.Public |
                            BindingFlags.Static |
                            BindingFlags.DeclaredOnly)
                    .Select(f => f.GetValue(null))
                    .Cast<T>();

    public override bool Equals(object obj)
    {
        if (obj is not Enumeration otherValue)
        {
            return false;
        }

        var typeMatches = GetType().Equals(obj.GetType());
        var valueMatches = Id.Equals(otherValue.Id);

        return typeMatches && valueMatches;
    }

    public override int GetHashCode() => Id.GetHashCode();

    public static int AbsoluteDifference(Enumeration firstValue, Enumeration secondValue)
    {
        var absoluteDifference = Math.Abs(firstValue.Id - secondValue.Id);
        return absoluteDifference;
    }

    public static T FromValue<T>(int value) where T : Enumeration
    {
        var matchingItem = Parse<T, int>(value, "value", item => item.Id == value);
        return matchingItem;
    }

    public static T FromDisplayName<T>(string displayName) where T : Enumeration
    {
        var matchingItem = Parse<T, string>(displayName, "display name", item => item.Name == displayName);
        return matchingItem;
    }

    private static T Parse<T, K>(K value, string description, Func<T, bool> predicate) where T : Enumeration
    {
        var matchingItem = GetAll<T>().FirstOrDefault(predicate);

        if (matchingItem == null)
            throw new InvalidOperationException($"'{value}' is not a valid {description} in {typeof(T)}");

        return matchingItem;
    }

    public int CompareTo(object other) => Id.CompareTo(((Enumeration)other).Id);
}

//public abstract class Enumeration<TEnum> : IEquatable<Enumeration<TEnum>>
//    where TEnum : Enumeration<TEnum>
//{
//    public int Id { get; protected set; }
//    public string Name { get; protected set; } = string.Empty;

//    protected Enumeration(int id, string name)
//    {
//        Id = id;
//        Name = name;
//    }

//    public static Dictionary<int, TEnum> GetValues()
//    {
//        var enumerationType = typeof(TEnum);

//        var fieldsForType = enumerationType
//            .GetFields(
//                BindingFlags.Public |
//                BindingFlags.Static |
//                BindingFlags.DeclaredOnly)
//            .Where(fieldInfo => enumerationType.IsAssignableFrom(fieldInfo.FieldType))
//            .Select(fieldInfo => (TEnum)fieldInfo.GetValue(default)!);

//        return fieldsForType.ToDictionary(x => x.Id);
//    }

//    public static IEnumerable<T> GetAll<T>() where T : Enumeration<T> =>
//        typeof(T).GetFields(BindingFlags.Public |
//                            BindingFlags.Static |
//                            BindingFlags.DeclaredOnly)
//                    .Select(f => f.GetValue(null))
//                    .Cast<T>();

//    public static TEnum? FromValue(int value)
//    {
//        return default;
//    }

//    public static TEnum? FromName(string name)
//    {
//        return default;
//    }

//    public bool Equals(Enumeration<TEnum>? other)
//    {
//        if (other == null) return false;

//        return GetType() == other.GetType() && Id == other.Id;
//    }

//    public override bool Equals(object obj)
//    {
//        return obj is Enumeration<TEnum> other &&
//            Equals(other);
//    }

//    public override int GetHashCode()
//    {
//        return Id.GetHashCode();
//    }
//}
