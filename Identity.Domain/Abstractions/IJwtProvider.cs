﻿using Identity.Domain.Customers;

namespace Identity.Domain.Abstractions;

public interface IJwtProvider
{
    Task<string> GenerateAsync(Customer customer);
}
