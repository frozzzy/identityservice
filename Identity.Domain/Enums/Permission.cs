﻿namespace Identity.Domain.Enums;
public enum Permission
{
    Reader = 1,
    Admin = 2
}
