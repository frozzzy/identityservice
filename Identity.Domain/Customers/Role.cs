﻿using Identity.Domain.SeedWork;

namespace Identity.Domain.Customers;
public class Role : Enumeration
{
    public static readonly Role Registered = new(1, "Registered");

    public Role(int id, string name) 
        : base(id, name)
    {
    }

    public ICollection<Permission> Permissions { get; set; }
    public ICollection<Customer> Customers { get; set; }
}
