﻿using Identity.Domain.Enums;
using Microsoft.AspNetCore.Authorization;

namespace Identity.Infrastructure.Authentification;
public class HasPermissionAttribute : AuthorizeAttribute
{
    public HasPermissionAttribute(Permission permission)
        : base(policy: permission.ToString())
    {
    }
}
