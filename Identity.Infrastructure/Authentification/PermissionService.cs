﻿using Microsoft.EntityFrameworkCore;

namespace Identity.Infrastructure.Authentification;
public class PermissionService : IPermissionService
{
    private readonly IdentityContext identityContext;

    public PermissionService(IdentityContext identityContext)
    {
        this.identityContext = identityContext;
    }

    public async Task<HashSet<string>> GetPermissionsAsync(Guid customerId)
    {
        var roles = await identityContext.Customers
            .Include(x => x.Roles)
                .ThenInclude(x => x.Permissions)
            .Where(x => x.Id == customerId)
            .Select(x => x.Roles)
            .ToArrayAsync();

        return roles
            .SelectMany(x => x)
            .SelectMany(x => x.Permissions)
            .Select(x => x.Name)
            .ToHashSet();
    }
}
