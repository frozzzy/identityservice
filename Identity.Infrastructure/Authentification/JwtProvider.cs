﻿using Identity.Domain.Abstractions;
using Identity.Domain.Customers;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Identity.Infrastructure.Authentification;
public class JwtProvider : IJwtProvider
{
    private readonly JwtOptions options;
    private readonly IPermissionService permissionService;

    public JwtProvider(IOptions<JwtOptions> jwtOptions, IPermissionService permissionService)
    {
        options = jwtOptions.Value;
        this.permissionService = permissionService;
    }

    public async Task<string> GenerateAsync(Customer customer)
    {
        var claims = new List<Claim> 
        {
            new(JwtRegisteredClaimNames.Sub, customer.Id.ToString()),
            new(JwtRegisteredClaimNames.Email, customer.Email)
        };

        var permissions = await permissionService
            .GetPermissionsAsync(customer.Id);

        foreach (var permission in permissions)
        {
            claims.Add(new(CustomClaims.Permissions, permission));
        }

        var singingCredentials = new SigningCredentials(
            new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(options.SecretKey)),
            SecurityAlgorithms.HmacSha256);

        var token = new JwtSecurityToken(
            options.Issuer,
            options.Audience,
            claims,
            null,
            DateTime.UtcNow.AddHours(1),
            singingCredentials);

        var tokenValue = new JwtSecurityTokenHandler()
            .WriteToken(token);

        return tokenValue;
    }
}
