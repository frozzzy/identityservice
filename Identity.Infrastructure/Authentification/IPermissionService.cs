﻿namespace Identity.Infrastructure.Authentification;
public interface IPermissionService
{
    Task<HashSet<string>> GetPermissionsAsync(Guid customerId);
}
