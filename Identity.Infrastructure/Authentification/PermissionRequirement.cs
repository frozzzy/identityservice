﻿using Microsoft.AspNetCore.Authorization;

namespace Identity.Infrastructure.Authentification;
public class PermissionRequirement : IAuthorizationRequirement
{
    public string Permission { get; }

    public PermissionRequirement(string permission)
    {
        Permission = permission;
    }
}
