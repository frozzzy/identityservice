﻿using Identity.Domain.Customers;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Identity.Infrastructure;
public class IdentityContext : DbContext
{
    public IdentityContext(DbContextOptions options) : base(options) { }

    public DbSet<Customer> Customers { get; set; }


    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }
}
