﻿using Identity.Domain.Enums;
using Identity.Infrastructure.Authentification;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Identity.API.Customers;
[Route("api/[controller]")]
[ApiController]
public class CustomerController : ControllerBase
{

    [HasPermission(Permission.Reader)]
    [HttpGet]
    public IActionResult About()
    {
        var coll = Enumerable.Range(0, 10);
        return Ok(coll);
    }
}
