﻿using Identity.API.Authentification.Commands;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Identity.API.Authentification;
[Route("api/[controller]")]
[ApiController]
public class AuthController : ControllerBase
{
    private readonly IMediator mediator;

    public AuthController(IMediator mediator)
    {
        this.mediator = mediator;
    }

    /// <summary>
    /// Авторизоваться и получить токен
    /// </summary>
    /// <param name="command"></param>
    /// <returns></returns>
    [HttpPost]
    [Route("Login")]
    public async Task<ActionResult<string>> Login([FromBody] LoginCommand command)
    {
        var result = await mediator.Send(command);

        return Ok(result);
    }
}
