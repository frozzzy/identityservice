﻿using Identity.Domain.Abstractions;
using Identity.Domain.Customers;
using Identity.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Identity.API.Authentification.Commands;

public record LoginCommand(string Email) : IRequest<string>;

public class LoginCommandHandler : IRequestHandler<LoginCommand, string>
{
    private readonly IJwtProvider jwtProvider;
    private readonly IdentityContext context;

    public LoginCommandHandler(IJwtProvider jwtProvider, IdentityContext context)
    {
        this.jwtProvider = jwtProvider;
        this.context = context;
    }

    public async Task<string> Handle(LoginCommand request, CancellationToken cancellationToken)
    {
        var customer = await context.Customers
            .FirstOrDefaultAsync(x => x.Email == request.Email, cancellationToken: cancellationToken);

        if (customer == null) throw new Exception("");


        var token = await jwtProvider.GenerateAsync(customer);

        return token;
    }
}
